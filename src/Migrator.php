<?php

namespace Drupal\cke5_youtube_migrator;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Psr\Log\LoggerAwareTrait;

/**
 * Service description.
 */
class Migrator {

  use LoggerChannelTrait;
  use LoggerAwareTrait;

  /**
   * Embed tags to search for.
   *
   * @var array<string, string>
   */
  private const EMBED_TAGS = [
    '<oembed' => '/<oembed>.*?<\/oembed>/is',
    '<div class="youtube-embed-wrapper"' => '/<div class="youtube-embed-wrapper".*?<\/div>/is',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Wysiwyg field finder service.
   *
   * @var \Drupal\cke5_youtube_migrator\WysiwygFieldFinder
   */
  protected WysiwygFieldFinder $wysiwygFieldFinder;

  /**
   * Constructs a Migrator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\cke5_youtube_migrator\WysiwygFieldFinder $wysiwyg_field_finder
   *   The Wysiwyg field finder service to search for wysiwyg fields.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, WysiwygFieldFinder $wysiwyg_field_finder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->wysiwygFieldFinder = $wysiwyg_field_finder;
  }

  /**
   * Return all known embed tags.
   */
  public function getEmbedTags() {
    return array_keys(self::EMBED_TAGS);
  }

  /**
   * Method description.
   */
  public function migrateEmbeds($entities) {
    $wysiwyg_fields = $this->wysiwygFieldFinder->getAllWysiwygFields();
    foreach ($entities as $entity) {
      if ($entity->isTranslatable()) {
        $languages = $entity->getTranslationLanguages();
        foreach ($languages as $language) {
          $translation = $entity->getTranslation($language->getId());
          $this->migrateEntityEmbedsInWysiwygFields($wysiwyg_fields, $translation);
        }
      }
      else {
        $this->migrateEntityEmbedsInWysiwygFields($wysiwyg_fields, $entity);
      }
    }
  }

  /**
   * Replace embed tags with drupal media.
   */
  public static function replaceWithDrupalMedia($matches) {
    $video_id_pattern = '/(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/watch\?.+&v=))([\w-]{11})/i';
    preg_match_all($video_id_pattern, $matches[0], $video_id_matches);
    if ($video_id_matches[1]) {
      $media = \Drupal::entityTypeManager()->getStorage('media')->create([
        'bundle' => 'remote_video',
        'field_media_oembed_video' => 'https://www.youtube.com/watch?v=' . $video_id_matches[1][0],
      ]);
      $media->save();
      /** @var \Drupal\media\Entity\Media $media */
      $uuid = $media->get('uuid')->getValue()[0]['value'];
      return '<drupal-media data-entity-type="media" data-entity-uuid="' . $uuid . '" data-align="center">&nbsp;</drupal-media>';
    }
  }

  /**
   * Migrate embeds in wysiwyg fields.
   *
   * @param array $wysiwyg_fields
   *   Wysiwyg fields to search in.
   * @param mixed $entity
   *   Entity to search in.
   */
  public function migrateEntityEmbedsInWysiwygFields(array $wysiwyg_fields, mixed $entity): void {
    if (isset($wysiwyg_fields[$entity->getEntityTypeId()][$entity->getType()])) {
      $fields = $wysiwyg_fields[$entity->getEntityTypeId()][$entity->getType()];
      foreach ($fields as $field) {
        foreach (self::EMBED_TAGS as $pattern) {
          $field_values = $entity->get($field)->getValue();
          if (isset($field_values[0]['value'])) {
            $field_values[0]['value'] = preg_replace_callback($pattern, [
              self::class,
              'replaceWithDrupalMedia',
            ], $field_values[0]['value']);

            $entity->set($field, $field_values);
            $entity->save();
          }
        }
      }
    }
  }

}
