<?php

namespace Drupal\cke5_youtube_migrator;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Service to get all wysiwyg fields from entities.
 */
class WysiwygFieldFinder {

  const WYSIWYG_FIELD_TYPES = ['string_long', 'text_with_summary', 'text_long'];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a FieldFinder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Get all wysiwyg fields from nodes and paragraphs.
   */
  public function getAllWysiwygFields() {
    return [
      'node' => $this->getNodeWysiwygFields(),
      'paragraph' => $this->getParagraphsWysiwygFields(),
    ];
  }

  /**
   * Get all wysiwyg fields from nodes.
   */
  public function getNodeWysiwygFields() {
    return $this->getFieldsFromEntityType('node', ['revision_log']);

  }

  /**
   * Get all wysiwyg fields from paragraphs.
   */
  public function getParagraphsWysiwygFields() {

    return $this->getFieldsFromEntityType('paragraph', [
      'revision_log',
      'behavior_settings',
    ]);

  }

  /**
   * Return fields that usually have wysiwyg editor enabled.
   *
   * Get an entity type ID, and return an array of fields of text long type.
   *
   * @param string $entity_type_id
   *   The entity type id to work with.
   * @param array $ommited_fields
   *   An array with fields to ommit from results.
   *
   * @return array
   *   An array with the bundle and all fields wysiwyg fields for each bundle.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getFieldsFromEntityType(string $entity_type_id, array $ommited_fields = []): array {
    $target_fields = [];
    // @todo improve this to get all fields that are wysiwyg, not only by type.
    $field_types_to_search = self::WYSIWYG_FIELD_TYPES;
    /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type_definition */
    $entity_type_definition = $this->entityTypeManager->getDefinition($entity_type_id);
    $bundle_entity_type = $entity_type_definition->getBundleEntityType();
    if ($bundle_entity_type) {
      $bundles = $this->entityTypeManager->getStorage($bundle_entity_type)
        ->loadMultiple();
      foreach ($bundles as $bundle) {
        $bundle_id = $bundle->getOriginalId();
        if (is_string($bundle_id)) {
          $fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle_id);
          $fields = array_filter($fields, static function ($field_definition) use ($field_types_to_search, $ommited_fields) {
            $type = $field_definition->getType();
            return (in_array($type, $field_types_to_search) && !in_array($field_definition->getName(), $ommited_fields, TRUE));
          });
          foreach ($fields as $field) {
            $target_fields[$bundle->id()][] = $field->getName();
          }
        }
      }
    }
    return $target_fields;
  }

}
