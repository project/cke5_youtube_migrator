<?php

namespace Drupal\cke5_youtube_migrator;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\path_alias\AliasManager;
use Drupal\path_alias\Entity\PathAlias;

/**
 * Service to generate report from entities with old embed methods in wysiwyg.
 */
class Reporter {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Patch alias.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  protected AliasManager $pathAliasManager;

  /**
   * Constructs a Reporter object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\path_alias\AliasManager $path_alias_manager
   *   Path alias.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    AliasManager $path_alias_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->pathAliasManager = $path_alias_manager;
  }

  /**
   * Generate report in csv and print it to console.
   *
   * @param array $entities
   *   Entities to report about.
   */
  public function doCsvReport(array $entities): void {
    $header = [];
    $header[] = [
      'Type',
      'Entity ID',
      'Langcode',
      'Referenced in: ID',
      'Referenced in: Title',
      'Referenced in: URL',
    ];
    $report = array_merge($header, $this->doReport($entities));
    $fp = fopen('php://output', 'wb');
    if ($fp) {
      foreach ($report as $fields) {
        fputcsv($fp, $fields);
      }
      fclose($fp);
    }
  }

  /**
   * Generate a report in php array.
   *
   * @param array $entities
   *   Entities to report about.
   *
   * @return array
   *   Report in PHP array format.
   */
  public function doReport(array $entities): array {
    $report = [];
    foreach ($entities as $entity) {
      /** @var \Drupal\Core\Entity\ContentEntityBase $entity */
      if ($entity->isTranslatable()) {
        $languages = $entity->getTranslationLanguages();
        foreach ($languages as $language) {
          $translation = $entity->getTranslation($language->getId());
          $this->doEntityReportRow($translation, $report);
        }
      }
      else {
        $this->doEntityReportRow($entity, $report);
      }
    }
    return $report;
  }

  /**
   * Get the node who references the given paragraph.
   *
   * @param \Drupal\Core\Entity\EntityInterface $paragraph
   *   Paragraph to search from.
   *
   * @return array
   *   Array of nodes.
   */
  public function getNodeFromParagraph(EntityInterface $paragraph): array {
    $data = [];
    $field_map = $this->entityFieldManager->getFieldMapByFieldType("entity_reference_revisions");
    // Prepare datas.
    foreach ($field_map as $entity_type => $fields) {
      foreach (array_keys($fields) as $field_name) {
        // Get with entity query all entities that reference $paragraph->id()
        $query = $this->entityTypeManager->getStorage($entity_type)->getQuery();
        /** @var string $field_name */
        $query->condition($field_name, $paragraph->id());
        $query->accessCheck(FALSE);
        $ids = $query->execute();
        if (is_array($ids)) {
          $entities = $this->entityTypeManager->getStorage($entity_type)
            ->loadMultiple($ids);
          foreach ($entities as $entity) {
            if ($entity->getEntityType()->id() === "node") {
              $data[] = $entity;
            }
            elseif ($entity->getEntityType()->id() === "paragraph") {
              $data = [...$data, ...$this->getNodeFromParagraph($entity)];
            }
          }
        }
      }
    }
    return $data;
  }

  /**
   * Get nodes url alias.
   *
   * @param int $entity_id
   *   Entity id.
   *
   * @return string
   *   Url alias.
   *
   * @todo make this work with all entities with url.
   */
  public function getUrlAlias(int $entity_id): string {
    return $this->pathAliasManager
      ->getAliasByPath('/node/' . $entity_id);
  }

  /**
   * Generate a report row for a given entity.
   *
   * @param mixed $entity
   *   Entity to report about.
   * @param array $report
   *   Report in PHP array format.
   */
  public function doEntityReportRow(mixed $entity, array &$report): void {
    $nodes = [];
    $entity_id = $entity->id() ?? 0;
    switch ($entity->getEntityType()->id()) {
      case "node":
        /** @var \Drupal\node\Entity\Node $entity */
        $report[] = [
          'type' => 'node',
          'entity_id' => $entity->id(),
          'language' => $entity->get('langcode')->getValue()[0]['value'],
          'related_entity_id' => '',
          'related_entity_title' => $entity->getTitle(),
          'related_entity_url' => $this->getUrlAlias($entity_id),
        ];
        return;

      case "paragraph":
        /** @var \Drupal\paragraphs\Entity\Paragraph $entity */
        $nodes = $this->getNodeFromParagraph($entity);
        if (!$nodes) {
          $report[] = [
            'type' => 'paragraph',
            'entity_id' => $entity->id(),
            'language' => $entity->get('langcode')->getValue()[0]['value'],
            'related_entity_id' => '-',
            'related_entity_title' => '-',
            'related_entity_url' => '-',
          ];
        }
        array_walk($nodes, function ($node) use (&$report, $entity) {
          $report[] = [
            'type' => 'paragraph',
            'entity_id' => $entity->id(),
            'language' => $entity->get('langcode')->getValue()[0]['value'],
            'related_entity_id' => $node->id(),
            'related_entity_title' => $node->getTitle(),
            'related_entity_url' => $node->toUrl()->setAbsolute()->toString(),
            // 'related_entity_url' => $this->getUrlAlias($node->id()),
          ];
        });
        return;
    }
  }

}
