<?php

namespace Drupal\cke5_youtube_migrator\Commands;

use Drupal\cke5_youtube_migrator\Cke5EmbedManager;
use Drush\Commands\DrushCommands;

/**
 * Commands to migrate ckeditor 5 embeds.
 */
class MigratorCommands extends DrushCommands {
  /**
   * Migrator helper service.
   *
   * @var \Drupal\ckeditor5_embed_migrator\Cke5EmbedManager
   */
  protected Cke5EmbedManager $cke5EmbedManager;

  /**
   * Construct the cke5EmbedManager commands.
   *
   * @param \Drupal\ckeditor5_embed_migrator\Cke5EmbedManager $cke5_embed_manager
   *   Migrator helper service.   *.
   */
  public function __construct(Cke5EmbedManager $cke5_embed_manager) {
    parent::__construct();
    $this->cke5EmbedManager = $cke5_embed_manager;
  }

  /**
   * Search and list all embeds found.
   *
   * @command cke5-embed-migrator:report
   * @aliases cke5-embed-migrator:r
   */
  public function doCsvReport() {
    $this->cke5EmbedManager->doCsvReport();
  }

  /**
   * Migrate embeds.
   *
   * @command cke5-embed-migrator:migrate
   * @aliases cke5-embed-migrator:m
   */
  public function migrateEmbeds() {
    $this->cke5EmbedManager->doMigrate();
  }

}
