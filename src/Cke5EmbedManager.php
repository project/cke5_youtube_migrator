<?php

namespace Drupal\cke5_youtube_migrator;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\ConditionInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Psr\Log\LoggerAwareTrait;

/**
 * Service description.
 */
class Cke5EmbedManager {
  use LoggerChannelTrait;
  use LoggerAwareTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Wysiwyg field finder service.
   *
   * @var \Drupal\cke5_youtube_migrator\WysiwygFieldFinder
   */
  protected WysiwygFieldFinder $wysiwygFieldFinder;


  /**
   * Reporter service.
   *
   * @var \Drupal\cke5_youtube_migrator\Reporter
   */
  protected Reporter $reporter;

  /**
   * Migrator service.
   *
   * @var \Drupal\cke5_youtube_migrator\Migrator
   */
  protected Migrator $migrator;

  /**
   * Constructs a Migrator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\cke5_youtube_migrator\WysiwygFieldFinder $wysiwyg_field_finder
   *   The Wysiwyg field finder service to search for wysiwyg fields..
   * @param \Drupal\cke5_youtube_migrator\Reporter $reporter
   *   The reporter service to generate reports.
   * @param \Drupal\cke5_youtube_migrator\Migrator $migrator
   *   The migrator service to migrate medias.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, WysiwygFieldFinder $wysiwyg_field_finder, Reporter $reporter, Migrator $migrator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->wysiwygFieldFinder = $wysiwyg_field_finder;
    $this->reporter = $reporter;
    $this->migrator = $migrator;
    $this->logger = $this->getLogger('cke5_youtube_migrator');
  }

  /**
   * Generate report in csv and print it to console.
   */
  public function doCsvReport() {
    $this->reporter->doCsvReport($this->getAllEntitiesWithEmbeds());
  }

  /**
   * Get all entities and migrate old embeds to medias.
   */
  public function doMigrate() {
    $entities = $this->getAllEntitiesWithEmbeds();
    $this->migrator->migrateEmbeds($entities);
    return $entities;
  }

  /**
   * Return all entities with embeds in fields.
   *
   * @return array
   *   Array of entities with embeds.
   */
  public function getAllEntitiesWithEmbeds(): array {
    $entities = [];
    $wysiwyg_fields_by_entity_type = $this->wysiwygFieldFinder->getAllWysiwygFields();
    foreach ($wysiwyg_fields_by_entity_type as $entity_type_id => $entity_type_fields) {
      foreach ($entity_type_fields as $bundle_id => $fields) {
        $embed_entities = $this->getEntitiesWithEmbedsFromEntityBundle($entity_type_id, $bundle_id, $fields);
        $entities = [...$entities, ...$embed_entities];
      }
    }
    return $entities;
  }

  /**
   * Return all entities of given entity type and bundle with embeds in fields.
   *
   * @param string $entity_type_id
   *   The entity type to search for.
   * @param int|string $bundle_id
   *   The bundle to search for.
   * @param mixed $fields
   *   Array of field names to search into.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of entities with embeds.
   */
  public function getEntitiesWithEmbedsFromEntityBundle(string $entity_type_id, int|string $bundle_id, mixed $fields): array {
    $entities = [];
    try {
      $entities_query = $this->entityTypeManager->getStorage($entity_type_id)
        ->getQuery();
      $entities_query->condition('type', $bundle_id);
      $embed_conditions = $this->getEmbedConditions($entities_query, $fields);
      $entities_query->condition($embed_conditions);
      $entities_query->accessCheck(FALSE);
      $entity_ids = $entities_query->execute();
      if (is_array($entity_ids)) {
        $entities = $this->entityTypeManager->getStorage($entity_type_id)
          ->loadMultiple($entity_ids);
      }
    }
    catch (PluginException $e) {
      $this->logger?->error($e->getMessage());
    }
    return $entities;
  }

  /**
   * Get embed conditions to filter content with embed tags.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $entites_query
   *   The query to add conditions to.
   * @param mixed $fields
   *   Array of field names to search into.
   *
   * @return \Drupal\Core\Entity\Query\ConditionInterface
   *   The query with the added conditions.
   */
  public function getEmbedConditions(QueryInterface $entites_query, mixed $fields): ConditionInterface {
    $embed_conditions = $entites_query->orConditionGroup();
    foreach ($fields as $field_name) {
      foreach ($this->migrator->getEmbedTags() as $embed_tag) {
        $embed_conditions->condition($field_name, '%' . $embed_tag . '%', 'LIKE');
      }
    }
    return $embed_conditions;
  }

}
